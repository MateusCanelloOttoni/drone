﻿using System;

namespace Algorithm.Logic.Models
{
    /// <summary>
    /// Representa o resultado de um processamento de uma instrução.
    /// </summary>
    [Serializable]
    public sealed class EvaluateResult
    {
        /// <summary>
        /// Posição a ser retornada para instruções consideradas inválidas.
        /// </summary>
        static readonly Point InvalidInputPosition = new Point(999, 999);

        /// <summary>
        /// Instância que determina um resultado inválido.
        /// </summary>
        internal static readonly EvaluateResult InvalidInputResult = new EvaluateResult()
        {
            IsInputValid = false,
            Position = EvaluateResult.InvalidInputPosition
        };

        internal EvaluateResult() { }

        public EvaluateResult(Point position)
        {
            this.Position = position;
            this.IsInputValid = true;
        }

        /// <summary>
        /// Determina se a instrução fornecida é considerada válida.
        /// </summary>
        public bool IsInputValid { get; private set; }

        /// <summary>
        /// Posição final do drone, calculada com base na instrução fornecida.
        /// </summary>
        public Point Position { get; private set; }

        /// <summary>
        /// Coordenada equivalente à posição final do drone, calculada com base na instrução fornecida.
        /// </summary>
        public string Coordinate
        {
            get
            {
                return this.Position.ToString();
            }
        }

        public override string ToString()
        {
            return this.Coordinate;
        }
    }
}