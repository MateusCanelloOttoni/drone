﻿using System;

namespace Algorithm.Logic.Models
{
    /// <summary>
    /// Representa uma coordenada em um plano cartesiano (X, Y).
    /// </summary>
    [Serializable]
    public struct Point
    {
        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Eixo X da coordenada cartesiana.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Eixo Y da coordenada cartesiana.
        /// </summary>
        public int Y { get; set; }
        
        public override string ToString()
        {
            return string.Format("({0:F0}, {1:F0})", this.X, this.Y);
        }
    }
}
