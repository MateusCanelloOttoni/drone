﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Algorithm.Logic.Tests
{
    [TestClass]
    public class DroneUnitTest_Additional
    {
        [TestMethod]
        public void Input_NNNLLL()
        {
            Assert.AreEqual("(3, 3)", Program.Evaluate("NNNLLL"));
        }

        [TestMethod]
        public void Input_NLNLNL()
        {
            Assert.AreEqual("(3, 3)", Program.Evaluate("NLNLNL"));
        }

        [TestMethod]
        public void Input_NNNXLLLXX()
        {
            Assert.AreEqual("(1, 2)", Program.Evaluate("NNNXLLLXX"));
        }

        [TestMethod]
        public void Input_NNL()
        {
            Assert.AreEqual("(1, 2)", Program.Evaluate("NNL"));
        }

        [TestMethod]
        public void Input_NNX2()
        {
            Assert.AreEqual("(999, 999)", Program.Evaluate("NNX2"));
        }

        [TestMethod]
        public void Input_N123LSX()
        {
            Assert.AreEqual("(1, 123)", Program.Evaluate("N123LSX"));
        }

        [TestMethod]
        public void Input_N123L()
        {
            Assert.AreEqual("(1, 123)", Program.Evaluate("N123L"));
        }

        [TestMethod]
        public void Input_NLS3X()
        {
            Assert.AreEqual("(1, 1)", Program.Evaluate("NLS3X"));
        }

        [TestMethod]
        public void Input_NL()
        {
            Assert.AreEqual("(1, 1)", Program.Evaluate("NL"));
        }

        [TestMethod]
        public void Input_N1N2S3S4L5L6O7O8X()
        {
            Assert.AreEqual("(4, -4)", Program.Evaluate("N1N2S3S4L5L6O7O8X"));
        }
    }
}
