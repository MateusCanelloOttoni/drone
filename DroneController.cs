﻿#region Using
using Algorithm.Logic.Models;
using System;
using System.Text.RegularExpressions;
#endregion

namespace Algorithm.Logic
{
    /// <summary>
    /// Representa o controlador de posicionamento do drone. 
    /// </summary>
    sealed class DroneController
    {
        // Tempo afixado em código por breviedade.
        private static readonly TimeSpan RegexDefaultTimeout = TimeSpan.FromSeconds(5);

        // Instância única do controlador.
        public static readonly DroneController Instance = new DroneController();
        
        // Relação de padrões de regex utilizados pela classe.
        const string NumberRangeValidationPatternString = @"\d+";
        const string NumberCancellationPattern = @"(N|L|O|S)\d+X";
        const string DirectionCancellationPattern = @"(N|L|S|O)X";
        const string RegexValidationPattern = @"^((N|L|O|S)+(\d*)|X*)+$";
        const string CommandParserPattern = @"N\d*|L\d*|S\d*|O\d*";

        // Maior número de passos válido.
        const int MaxValidStepNumber = 2147483647;

        // Em função de ser um singleton, trabalha-se com instâncias da classe Regex
        // ao invés dos métodos estáticos fornecidos pela classe para otimizar 
        // a performance geral.
        private readonly Regex ValidationRegex;
        private readonly Regex NumberCancellationRegex;
        private readonly Regex NumberRangeValidationRegex;
        private readonly Regex DirectionCancellationRegex;
        private readonly Regex CommandParserRegex;

        private DroneController()
        {
            this.ValidationRegex = new Regex(
                DroneController.RegexValidationPattern,
                RegexOptions.Singleline,
                DroneController.RegexDefaultTimeout);

            this.NumberCancellationRegex = new Regex(
                DroneController.NumberCancellationPattern,
                RegexOptions.Singleline,
                DroneController.RegexDefaultTimeout);

            this.NumberRangeValidationRegex = new Regex(
                DroneController.NumberRangeValidationPatternString,
                RegexOptions.Singleline,
                DroneController.RegexDefaultTimeout);

            this.DirectionCancellationRegex = new Regex(
                DroneController.DirectionCancellationPattern,
                RegexOptions.Singleline,
                DroneController.RegexDefaultTimeout);

            this.DirectionCancellationRegex = new Regex(
                DroneController.DirectionCancellationPattern,
                RegexOptions.Singleline,
                DroneController.RegexDefaultTimeout);

            this.CommandParserRegex = new Regex(
                DroneController.CommandParserPattern,
                RegexOptions.Singleline,
                DroneController.RegexDefaultTimeout);
        }

        /// <summary>
        /// Avalia a instrução fornecida e devolve o resultado equivalente ao processamento da mesma.
        /// </summary>
        /// <param name="input">Instrução a ser avaliada e processada.</param>
        /// <returns>Retorna o resultado obtido com base na instrução informada.</returns>
        public EvaluateResult Evaluate(string input)
        {
            if (!this.IsValidInput(input))
            {
                return EvaluateResult.InvalidInputResult;
            }

            Point position = this.DeterminePosition(input);
            return new EvaluateResult(position);
        }

        /// <summary>
        /// Determina se a instrução fornecida para processamento é considerada válida.
        /// </summary>
        /// <param name="input">Instrução a ser fornecida para processamento.</param>
        /// <returns>Retorna verdadeiro se a instrução fornecida é considerada válida; caso contrário, retorna falso.</returns>
        public bool IsValidInput(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            // Valida se o conteúdo do string possui apenas caracteres (dígitos numéricos/letras) válidos.
            bool isValidPattern = this.ValidationRegex.IsMatch(input);
            if (!isValidPattern)
            {
                return false;
            }

            // Remove os cancelamentos.
            input = this.SimplifyInput(input);

            // Determina se os números informados estão dentro do range permitido.
            bool areAllNumbersValid = true;
            foreach (Match numberMatch in this.NumberRangeValidationRegex.Matches(input))
            {
                int number = default(int);
                // Converte para número para poder validar o valor máximo.
                if (int.TryParse(numberMatch.Value, out number))
                {
                    if (number >= DroneController.MaxValidStepNumber)
                    {
                        areAllNumbersValid = false;
                    }
                }
                else
                {
                    areAllNumbersValid = false;
                }

                if (!areAllNumbersValid)
                {
                    // Não precisa validar todos os números se um deles já não é válido.
                    break;
                }
            }

            return areAllNumbersValid;
        }

        /// <summary>
        /// Simplifica a instrução fornecida removendo os comandos precedidos por cancelamentos.
        /// </summary>
        /// <param name="input">Instrução fornecida para processamento.</param>
        /// <returns>Retorna a instrução simplificada.</returns>
        private string SimplifyInput(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentNullException(nameof(input));
            }

            // Remove as direções seguidas de dígitos numéricos que serão cancelados.
            input = this.NumberCancellationRegex.Replace(input, string.Empty);

            // Remove os comandos seguidos de identificador de cancelamento.
            while (this.DirectionCancellationRegex.IsMatch(input))
            {
                input = this.DirectionCancellationRegex.Replace(input, string.Empty);
            }

            return input;
        }

        /// <summary>
        /// Efetua o processamento propriamente dito da instrução fornecida para execução.
        /// </summary>
        /// <param name="input">Instrução fornecida para execução.</param>
        /// <returns>Retorna a posição determinada a partir da instrução fornecida.</returns>
        private Point DeterminePosition(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentNullException(nameof(input));
            }

            // Remove os comandos cancelados.
            input = this.SimplifyInput(input);

            // Acumula os valores de X e Y.
            int x = 0;
            int y = 0;

            // Obtém os grupos de comandos.
            foreach (Match match in this.CommandParserRegex.Matches(input))
            {
                // Quando não houver um número de passos após a direção, deve se considerar como 1 passo.
                int steps = 1;

                if (match.Value.Length > 1)
                {
                    // Se o grupo possuir mais de um caractere, significa que se trata
                    // de uma direção (N, S, L, O) seguida por um número determinado de passos.
                    steps = int.Parse(match.Value.Substring(1, match.Value.Length - 1));
                }

                // A direção (N, S, L, O) sempre inicia a string.
                char direction = match.Value[0];

                switch (direction)
                {
                    case 'N':
                        y += steps;
                        break;

                    case 'S':
                        y -= steps;
                        break;

                    case 'L':
                        x += steps;
                        break;

                    case 'O':
                        x -= steps;
                        break;
                }
            }

            return new Point(x, y);
        }
    }
}
